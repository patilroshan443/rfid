-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 19, 2017 at 06:15 PM
-- Server version: 8.0.0-dmr-log
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rfid`
--

-- --------------------------------------------------------

--
-- Table structure for table `college_entry`
--

CREATE TABLE `college_entry` (
  `rfidtag` varchar(50) NOT NULL,
  `Time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `college_entry`
--

INSERT INTO `college_entry` (`rfidtag`, `Time`) VALUES
('090073EF4DD8', '19-04-2017 21:28'),
('090073EF4DD8', '19-04-2017 21:32');

-- --------------------------------------------------------

--
-- Stand-in structure for view `college_entry_data`
--
CREATE TABLE `college_entry_data` (
`rfidtag` varchar(50)
,`year` varchar(500)
,`department` varchar(500)
);

-- --------------------------------------------------------

--
-- Table structure for table `college_out`
--

CREATE TABLE `college_out` (
  `id` int(11) NOT NULL,
  `rfidtag` varchar(50) NOT NULL,
  `Time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `college_out`
--

INSERT INTO `college_out` (`id`, `rfidtag`, `Time`) VALUES
(8, '090073EF4DD8', '19-04-2017 21:29'),
(9, '090073EF4DD8', '19-04-2017 21:32');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `flag` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `username`, `password`, `flag`) VALUES
(1, 'pranali', 'pranali', ''),
(2, 'admin', 'admin', '');

-- --------------------------------------------------------

--
-- Table structure for table `rfid_student`
--

CREATE TABLE `rfid_student` (
  `stud_id` int(11) NOT NULL,
  `rfidtag` varchar(500) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `department` varchar(500) NOT NULL,
  `year` varchar(500) NOT NULL,
  `division` varchar(500) NOT NULL,
  `rollno` varchar(500) NOT NULL,
  `mobileno` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `address` varchar(50) NOT NULL,
  `location` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rfid_student`
--

INSERT INTO `rfid_student` (`stud_id`, `rfidtag`, `firstname`, `lastname`, `department`, `year`, `division`, `rollno`, `mobileno`, `email`, `dob`, `address`, `location`) VALUES
(10, '090073EF4DD8', 'Sayali', 'Rane', 'Computer Engineering', 'BE', 'A', '52', '8862078210', 'patilroshan443@gmail.com', '1995-06-19', 'Flat No- 2, Sairam appt., Ganesh baba nagar, Behin', 'Localite'),
(12, '0900732EAEFA', 'Prachi', 'Chaudhari', 'Computer Engineering', 'BE', 'A', '22', '9822529906', 'prachichaudhari@gmail.com', '2017-01-04', 'vdvfs', 'Hostelite');

-- --------------------------------------------------------

--
-- Structure for view `college_entry_data`
--
DROP TABLE IF EXISTS `college_entry_data`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `college_entry_data`  AS  select `college_entry`.`rfidtag` AS `rfidtag`,`rfid_student`.`year` AS `year`,`rfid_student`.`department` AS `department` from (`college_entry` join `rfid_student` on((`college_entry`.`rfidtag` = `rfid_student`.`rfidtag`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `college_out`
--
ALTER TABLE `college_out`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rfid_student`
--
ALTER TABLE `rfid_student`
  ADD PRIMARY KEY (`stud_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `college_out`
--
ALTER TABLE `college_out`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `rfid_student`
--
ALTER TABLE `rfid_student`
  MODIFY `stud_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
